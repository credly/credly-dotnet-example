﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace WebRequest
{ 
    public class Program
    {
        private static IConfigurationRoot configuration;

        public static void Main(string[] args)
        {

            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "env.json");

            configuration = configurationBuilder.Build();

            if (args.ElementAtOrDefault(0) == null) {
                throw new System.ArgumentException("Specify the operation");
            }

            var operation = args[0];

            var parameters = new[] {
                new Dictionary<string, string> { { "access_token", configuration["accessToken"] } },
                new Dictionary<string, string> { { "use_shim", configuration["useShim"] } }
            };

            if (operation == "delete") {           
                if (args.ElementAtOrDefault(1) != null) {
                    var emailId = args[1];
                    SendDeleteRequest($"me/emails/{emailId}", parameters).Wait();
                } else {
                    throw new System.ArgumentException("Specify the ID of the email address to delete");
                }
            } else {
                SendGetRequest($"me/emails", parameters).Wait();
            } 
        }

        // Method to print JSON in a human-readable format.
        private static void PrettyPrint(string output)
        {
            Console.WriteLine(
                JsonConvert.SerializeObject(
                    JsonConvert.DeserializeObject(output),
                    Formatting.Indented
                )
            );
        }

        private static string DecorateRequest(HttpClient client, string endpoint, Dictionary<string, string>[] parameters = null)
        {
            client.BaseAddress = new Uri(configuration["credlyBaseUrl"]);

            client.DefaultRequestHeaders.Add("X-Api-Key", configuration["apiKey"]);
            client.DefaultRequestHeaders.Add("X-Api-Secret", configuration["apiSecret"]);

            var requestUri = "/" + configuration["credlyApiVersion"] + "/" + endpoint;

            if (parameters != null) {
                foreach (Dictionary<string, string> param in parameters) {
                    requestUri = QueryHelpers.AddQueryString(requestUri, param);
                }
            }

            Console.WriteLine("Requested: " + requestUri);

            return requestUri;
        }

        private static async Task SendDeleteRequest(string endpoint, Dictionary<string, string>[] parameters = null)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var requestUri = DecorateRequest(client, endpoint, parameters);
                    var response   = await client.DeleteAsync(requestUri);

                    response.EnsureSuccessStatusCode();

                    PrettyPrint(await response.Content.ReadAsStringAsync());
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }
            }
        }

        private static async Task SendGetRequest(string endpoint, Dictionary<string, string>[] parameters = null)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var requestUri = DecorateRequest(client, endpoint, parameters);
                    var response   = await client.GetAsync(requestUri);

                    response.EnsureSuccessStatusCode();

                    PrettyPrint(await response.Content.ReadAsStringAsync());
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }
            }
        }
    }
}